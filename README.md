# Handles encoding and moving video files from one bucket S3 to another

## Installation (using composer)

Add following to `repositories` section of your composer.json 
```json
"repositories": [
    // ...
    {
        "type": "vcs",
        "url": "https://bitbucket.org/fluxtechnologieshub/laravel-amazon-manager"
    }
],
```

Add following to `require` section of your composer.json 
```json
"require": {
    // ...
    "fluxtechnologieshub/laravel-amazon-manager": "dev-master"
},
```

And run `composer update`

### Usage

In Laravel find the `providers` key in your config/app.php and register the AWS Service Provider.
```php
'providers' => [
    // ...
    Aws\Laravel\AwsServiceProvider::class,
]
```

Find the `aliases` key in your config/app.php and add the AWS facade alias.
```php
'aliases' => [
    // ...
    'AWS' => Aws\Laravel\AwsFacade::class,
]
```

Update your `settings` in the generated app/config/aws.php configuration file.
```php
return [
    'credentials' => [
        'key'    => 'YOUR_AWS_ACCESS_KEY_ID',
        'secret' => 'YOUR_AWS_SECRET_ACCESS_KEY',
    ],
    'region' => 'us-west-2',
    'version' => 'latest',
    
    // You can override settings for specific services
    'Ses' => [
        'region' => 'us-east-1',
    ],
];
```

In your controller
```php
<?php

namespace App\Http\Controllers;

use FluxTech\AmazonManager\Manager;
use App\Http\Models\Media;

class AmazonController extends Controller
{
    public function handle()
    {
        $amazonManager = new Manager(AWS::createClient('s3'), AWS::createClient('ElasticTranscoder'));
        $amazonManager->handle(Media::class);
    }
}
```
