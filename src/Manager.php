<?php

namespace FluxTech\AmazonManager;

use Aws\S3\S3MultiRegionClient;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Illuminate\Database\Eloquent\Model;
use DateTime;
use Illuminate\Support\Collection;

/**
 * Class Manager - Handles encoding and moving videos files from one bucket S3 to another
 * @package App\Amazon
 */
class Manager
{
    /**
     * @var S3MultiRegionClient
     */
    private $s3;
    /**
     * @var ElasticTranscoderClient
     */
    private $transCoder;

    const HLS_PRESETS = [
        'hls240p' => '1484561878158-bepru2',
        'hls480p' => '1467708368360-z2zqnc',
        'hls2000k' => '1476889116706-es50l6',
        'hlsFullHD' => '1476888754206-tfneyw'
    ];
    const HASH_ALGORITHM = 'sha256';
    const BUCKET_INPUT = 'input';
    const BUCKET_OUTPUT = 'output';
    const BUCKET_SHORT_KEY = "short/%s/30s.mp4";
    const BUCKET_HLS_KEY = 'hls/%s/';
    const BUCKET_OUTPUT_KEY = 'outputs/%s/outputs/%s/';
    const PRESET_ID = '1351620000001-000010';
    const PIPELINE_ID = '1446827273819-lpl4jm';
    const AMAZON_LOCATION = 'Amazon (S3)';
    const OBJECT_DONE_KEY = 'done/%s.mp4';
    const OUTPUT_TIME_START = '0';
    const OUTPUT_TIME_DURATION = '00:00:30.000';
    const SEGMENT_DURATION = '2';
    const THUMBNAIL_PATTERN = '{count}-thumbs';
    const PLAYLIST_NAME = 'hls_%s';
    const PLAYLIST_FORMAT = 'HLSv3';

    /**
     * Manager constructor.
     * @param S3MultiRegionClient $s3Client
     * @param ElasticTranscoderClient $elasticTransCoderClient
     */
    public function __construct(S3MultiRegionClient $s3Client, ElasticTranscoderClient $elasticTransCoderClient)
    {
        $this->s3 = $s3Client;
        $this->transCoder = $elasticTransCoderClient;
    }

    /**
     * @param $model - Media Class Name
     */
    public function handle($model)
    {
        /** @var Model $model */
        $this->getObjectList()->each(function($object) use ($model) {
            if (substr($object['Key'], 0, 7) != "medias/") {
                return; // continue loop
            }
            $mediaId = self::getMediaIdByKey($object['Key']);
            /** @var Model $media */
            $media = $model::query()->find($mediaId);
            if (!$media) {
                return; // continue loop
            }
            $secureId = self::getSecureIdByKey($object['Key']);
            $this->deleteObjects($secureId, $mediaId);
            $this->putObject($mediaId);
            $this->createTransCoderJob($object['Key'], $mediaId);
            $jobData = $this->createTransCoderJobRequest($object['Key'], $secureId, $mediaId);
            $this->copyObject($object['Key'], $mediaId);
            self::saveModel($media, collect([
                'm3u8' => $secureId,
                'id' => $jobData['Id']
            ]));
        });
    }

    /**
     * @return \Illuminate\Support\Collection|array
     */
    private function getObjectList()
    {
        return collect($this->s3->getIterator('ListObjects', ['Bucket' => self::BUCKET_INPUT]));
    }

    /**
     * @param $secureId
     * @param $mediaId
     */
    private function deleteObjects($secureId, $mediaId)
    {
        $this->s3->deleteObject([
            'Bucket' => self::BUCKET_OUTPUT,
            'Key' => sprintf(self::BUCKET_SHORT_KEY, $mediaId)
        ]);
        $this->s3->deleteMatchingObjects(self::BUCKET_OUTPUT, sprintf(self::BUCKET_OUTPUT_KEY, $secureId, $mediaId));
        $this->s3->deleteMatchingObjects(self::BUCKET_OUTPUT, sprintf(self::BUCKET_HLS_KEY, $mediaId));
    }

    /**
     * @param $mediaId
     */
    private function putObject($mediaId)
    {
        $this->s3->putObject([
            'Bucket' => self::BUCKET_OUTPUT,
            'Key' => sprintf(self::BUCKET_HLS_KEY, $mediaId),
            'Body' => "",
            'ACL' => 'public-read'
        ]);
    }

    /**
     * @param $key
     * @param $mediaId
     */
    private function copyObject($key, $mediaId)
    {
        if ($this->s3->doesObjectExist(self::BUCKET_INPUT, $key)) {
            $this->s3->copyObject([
                'Bucket' => self::BUCKET_INPUT,
                'Key' => sprintf(self::OBJECT_DONE_KEY, $mediaId),
                'CopySource' => sprintf('%s/%s', self::BUCKET_INPUT, $key),
            ]);
        }
    }

    /**
     * @param $key
     * @param $mediaId
     */
    private function createTransCoderJob($key, $mediaId)
    {
        $this->transCoder->createJob([
            'PipelineId' => 'pipelineId',
            'Input' => [
                'Key' => $key
            ],
            'Output' => [
                'Key' => sprintf(self::BUCKET_SHORT_KEY, $mediaId),
                'PresetId' => self::PRESET_ID,
                'Composition' => [
                    [
                        'TimeSpan' => [
                            'StartTime' => self::OUTPUT_TIME_START,
                            'Duration' => self::OUTPUT_TIME_DURATION
                        ]
                    ]
                ],
            ],
        ]);
    }

    /**
     * @param $key
     * @param $secureId
     * @param $mediaId
     * @return mixed|null
     */
    private function createTransCoderJobRequest($key, $secureId, $mediaId)
    {
        $outputs = collect([]);
        foreach (self::HLS_PRESETS as $prefix => $preset_id) {
            $outputs->push([
                'Key' => sprintf('%s/%s', $prefix, $secureId),
                'PresetId' => $preset_id,
                'SegmentDuration' => self::SEGMENT_DURATION,
                'ThumbnailPattern' => self::THUMBNAIL_PATTERN
            ]);
        }
        $create_job_request = [
            'PipelineId' => self::PIPELINE_ID,
            'Input' => [
                'Key' => $key
            ],
            'Outputs' => $outputs->toArray(),
            'OutputKeyPrefix' => sprintf('%s%s/', sprintf(self::BUCKET_HLS_KEY, $mediaId), $secureId),
            'Playlists' => [
                [
                    'Name' => sprintf(self::PLAYLIST_NAME, $secureId),
                    'Format' => self::PLAYLIST_FORMAT,
                    'OutputKeys' => $outputs->map(function($object) {
                        return $object['Key'];
                    })->toArray()

                ]
            ]
        ];
        return $this->transCoder->createJob($create_job_request)->get('Job');
    }

    /**
     * @param $key
     * @return string
     */
    private static function getSecureIdByKey($key)
    {
        return hash(self::HASH_ALGORITHM, utf8_encode($key)) . uniqid();
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function getMediaIdByKey($key)
    {
        return str_replace('.mp4', '', str_replace('medias/', '', $key));
    }

    /**
     * @param Model $model
     * @param Collection $attr
     */
    public static function saveModel(Model $model, Collection $attr)
    {
        $model->transcoder_jobhls_m3u8 = $attr->get('m3u8');
        $model->transcoder_jobhls_id = '';
        $model->transcoder_jobhls_status = 'Submitted';
        $model->transcoder_jobhls_id = $attr->get('id');
        $model->transcoder_job_date = (new DateTime())->format('Y-m-j H:i:s');
        $model->uploaded = 1;
        if (!strstr($model->location, self::AMAZON_LOCATION)) {
            $model->location .= sprintf("\r\n%s", self::AMAZON_LOCATION);
        }
        $model->save();
    }
}